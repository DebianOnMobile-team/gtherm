/*
 * Copyright (C) 2019 Purism SPC
 * SPDX-License-Identifier: LGPL-2.1+
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#include "gth-thermal-zone.h"

/**
 * SECTION:gth-thermal-zone
 * @short_description: A thermal zone
 * @Title: GthThermalZone
 *
 * The #GthThermalZone represents a thermal zone that
 * measures a temperature and reacts on trip points.
 */

typedef struct _GthThermalZone {
  GthGdbusThermalZoneProxy parent;

} GthThermalZone;

G_DEFINE_TYPE (GthThermalZone, gth_thermal_zone, GTH_GDBUS_TYPE_THERMAL_ZONE_PROXY);

static void
gth_thermal_zone_class_init (GthThermalZoneClass *klass)
{
}

static void
gth_thermal_zone_init (GthThermalZone *self)
{
}

/**
 * gth_thermal_zone_get_path:
 * @self: A #GthThermalzone.
 *
 * Gets the DBus path of the #GthThermalZone object.
 *
 * Returns: (transfer none): The DBus path of the #GthThermalZone object.
 */
const gchar *
gth_thermal_zone_get_path (GthThermalZone *self)
{
    g_return_val_if_fail (GTH_IS_THERMAL_ZONE (self), NULL);

    return g_dbus_proxy_get_object_path (G_DBUS_PROXY (self));
}

/**
 * gth_thermal_zone_get_type_:
 * @self: A #GthThermalzone.
 *
 * Gets the type of the #GthThermalZone.
 *
 * Returns: (transfer none): The type of the #GthThermalZone.
 */
const gchar *
gth_thermal_zone_get_type_ (GthThermalZone *self)
{
  g_return_val_if_fail (GTH_IS_THERMAL_ZONE (self), NULL);

  return gth_gdbus_thermal_zone_get_type_ (
		 GTH_GDBUS_THERMAL_ZONE (self));
}

/**
 * gth_thermal_zone_get_temperature:
 * @self: A #GthThermalzone.
 *
 * Gets the current temperature of the #GthThermalZone.
 *
 * Returns: The #GthThermalZone's temperature.
 */
gint
gth_thermal_zone_get_temperature (GthThermalZone *self)
{
  g_return_val_if_fail (GTH_IS_THERMAL_ZONE (self), 0);

  return gth_gdbus_thermal_zone_get_temperature (
		 GTH_GDBUS_THERMAL_ZONE (self));
}


/**
 * gth_thermal_zone_get_cooling_devices_dbus_paths
 * @self: A #GthThermalzone.
 *
 * Gets the DBus paths of the cooling devices in the #GthThermalZone.
 *
 * Returns: The #GthThermalZone's cooling device's DBus paths
 */
const gchar *const *
gth_thermal_zone_get_cooling_devices_dbus_paths (GthThermalZone *self)
{
  g_return_val_if_fail (GTH_IS_THERMAL_ZONE (self), 0);

  return gth_gdbus_thermal_zone_get_cooling_devices (
		 GTH_GDBUS_THERMAL_ZONE (self));
}

