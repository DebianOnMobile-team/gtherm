/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */
#pragma once

#if !defined (__LIBGTHERM_H_INSIDE__) && !defined (LIBGTHERM_COMPILATION)
#error "Only <libgtherm.h> can be included directly."
#endif

#include "gth-gdbus.h"
#include <glib-object.h>

G_BEGIN_DECLS

#define GTH_TYPE_MANAGER (gth_manager_get_type())

G_DECLARE_FINAL_TYPE (GthManager, gth_manager, GTH, MANAGER, GthGdbusObjectManagerClient)

GthManager *gth_manager_new_sync (GDBusConnection                *connection,
				  GDBusObjectManagerClientFlags   flags,
				  GCancellable                   *cancellable,
				  GError                        **error);
GPtrArray  *gth_manager_get_cooling_devices (GthManager *self);
GPtrArray  *gth_manager_get_thermal_zones (GthManager *self);
G_END_DECLS
