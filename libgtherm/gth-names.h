/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#pragma once

#define GTH_DBUS_NAME "org.sigxcpu.Thermal"
#define GTH_DBUS_PATH "/org/sigxcpu/Thermal"

#ifdef GTH_TEST
# define GTH_DBUS_TYPE G_BUS_TYPE_SESSION
#else
# define GTH_DBUS_TYPE G_BUS_TYPE_SYSTEM
#endif

#define GTH_DBUS_IFACE_THERMAL_ZONE GTH_DBUS_NAME ".ThermalZone"
#define GTH_DBUS_IFACE_COOLING_DEVICE GTH_DBUS_NAME ".CoolingDevice"
