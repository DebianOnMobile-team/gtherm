#!/usr/bin/python3
#
# export GI_TYPELIB_PATH=/usr/local/lib/x86_64-linux-gnu/girepository-1.0/

import gi
gi.require_version('Gio', '2.0')
from gi.repository import Gio
gi.require_version('GTherm', '0.0')
from gi.repository import GTherm

con = Gio.bus_get_sync(Gio.BusType.SESSION, None)
manager = GTherm.Manager.new_sync(con, 0, None)

print("Cooling devices")
for d in manager.get_cooling_devices():
    print("{}".format(d))

print("Thermal zones")
for d in manager.get_thermal_zones():
    print("{}".format(d))
