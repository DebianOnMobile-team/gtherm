Thermal information for GNOME
=============================

gtherm provides a DBus daemon (gthd) to monitor thermal zones and cooling
devices. It offers a library (libgtherm) and GObject introspection bindings to
ease reading the values in applications.

## License

gtherm is licensed under the GPLv3+ while the libgtherm library is licensed
under LGPL 2.1+.

## Getting the source

```sh
git clone https://source.puri.sm/Librem5/gtherm
cd gtherm
```

The master branch has the current development version.

## Dependencies
On a Debian based system run

```sh
sudo apt-get -y install build-essential
sudo apt-get -y build-dep .
```

For an explicit list of dependencies check the `Build-Depends` entry in the
[debian/control][] file.

## Building

We use the meson (and thereby Ninja) build system for gtherm.  The quickest
way to get going is to do the following:

    meson . _build
    ninja -C _build
    ninja -C _build install

## Running
### Running from the source tree
To run the daemon use

```sh
_build/src/gthd
```

You can then see temperature changes via

```sh
gdbus monitor --session --dest org.sigxcpu.Thermal
```

See `examples/` for a simple python example using GObject introspection.

[debian/control]: https://source.puri.sm/Librem5/gtherm/blob/master/debian/control#L5
