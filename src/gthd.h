#pragma once

#include <glib-object.h>

#define GTHD_THERMAL_SYSFS "/sys/class/thermal"
#define GTHD_THERMAL_SYSFS_INTERVAL 10

G_BEGIN_DECLS

typedef enum {
    GTHD_ERROR_FAILED = 0,
} GthdError;

GQuark gthd_error_quark (void);

G_END_DECLS
