/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0+
 */
#pragma once

#include "gth-gdbus.h"
#include <glib-object.h>

G_BEGIN_DECLS

#define GTHD_THERMAL_ZONE_SYSFS "thermal_zone"

#define GTHD_TYPE_THERMAL_ZONE (gthd_thermal_zone_get_type())

G_DECLARE_FINAL_TYPE (GthdThermalZone, gthd_thermal_zone, GTHD, THERMAL_ZONE, GthGdbusThermalZoneSkeleton);

GthdThermalZone *gthd_thermal_zone_new (const gchar *name, GError **error);
gint             gthd_thermal_zone_get_id (GthdThermalZone *self);
const gchar     *gthd_thermal_zone_get_name (GthdThermalZone *self);
gchar *          gthd_thermal_zone_get_dbus_path (GthdThermalZone *self);

G_END_DECLS
