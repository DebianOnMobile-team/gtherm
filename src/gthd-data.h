/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0+
 */
#pragma once

#include "gthd-cooling-device.h"
#include "gthd-thermal-zone.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define GTHD_TYPE_DATA (gthd_data_get_type())

G_DECLARE_FINAL_TYPE (GthdData, gthd_data, GTHD, DATA, GObject);

GthdData          *gthd_data_get_default (void);
const GPtrArray   *gthd_data_get_cooling_devs   (GthdData *self);
GthdCoolingDevice *gthd_data_get_cooling_device (GthdData *self, const char *name);
const GPtrArray   *gthd_data_get_thermal_zones  (GthdData *self);
GthdThermalZone   *gthd_data_get_thermal_zone   (GthdData *self, const char *name);

G_END_DECLS
