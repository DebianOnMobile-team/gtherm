/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0+
 */
#pragma once

#include "gth-gdbus.h"
#include <glib-object.h>

G_BEGIN_DECLS

#define GTHD_COOLING_DEVICE_SYSFS "cooling_device"

#define GTHD_TYPE_COOLING_DEVICE (gthd_cooling_device_get_type())

G_DECLARE_FINAL_TYPE (GthdCoolingDevice, gthd_cooling_device, GTHD, COOLING_DEVICE, GthGdbusCoolingDeviceSkeleton);

GthdCoolingDevice *gthd_cooling_device_new           (const gchar *name, GError **error);
gint               gthd_cooling_device_get_id        (GthdCoolingDevice *self);
const gchar*       gthd_cooling_device_get_name      (GthdCoolingDevice *self);
gchar *            gthd_cooling_device_get_dbus_path (GthdCoolingDevice *self);

G_END_DECLS
